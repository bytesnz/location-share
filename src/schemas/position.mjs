export default {
	type: 'object',
	title: 'Position',
	properties: {
		time: {
			type: 'string',
			title: 'Time',
			format: 'date-time'
		},
		latitude: {
			type: 'number',
			title: 'Latitude',
			unit: '°'
		},
		longitude: {
			type: 'number',
			title: 'Longitude',
			unit: '°'
		},
		altitude: {
			type: 'number',
			title: 'Altitude',
			unit: 'm'
		},
		accuracy: {
			type: 'number',
			title: 'Accuracy',
			unit: 'm'
		},
		altitudeAccuracy: {
			type: 'number',
			title: 'Altitude Accuracy',
			unit: 'm'
		},
		heading: {
			type: 'number',
			title: 'Bearing',
			unit: '°'
		},
		speed: {
			type: 'number',
			title: 'Speed',
			unit: 'm/s'
		}
	},
	required: ['time', 'latitude', 'longitude']
};
