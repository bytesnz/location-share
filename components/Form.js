import { Button, StyleSheet, Text, TextInput, ScrollView, View } from 'react-native';
import { useState, useEffect } from 'react';
import URLParse from 'url-parse';
import globalStyles from '../globalStyles';

export default function Form(props) {
	const [label, setLabel] = useState(props.share?.label);
	const [url, setUrl] = useState(props.share?.url);
	const [token, setToken] = useState(props.share?.token);
	const [updateTime, setUpdateTime] = useState(
		props.share?.updateTime ? String(props.share.updateTime / 1000) : null
	);
	const [updateDistance, setUpdateDistance] = useState(
		props.share?.updateDistance ? String(props.share?.updateDistance) : null
	);
	const [timeout, setTimeout] = useState(
		props.share?.timeout ? String(props.share?.timeout / 1000) : null
	);
	const [maximumAge, setMaximumAge] = useState(
		props.share?.maximumAge ? String(props.share?.maximumAge / 1000) : null
	);
	const [urlStatus, setUrlStatus] = useState(null);

	useEffect(() => {
		if (url) {
			checkUrl();
		}
	}, []);

	const updateUrl = (value) => {
		setUrl(value);
		if (urlStatus !== null) {
			setUrlStatus(null);
		}
	};

	const checkUrl = () => {
		const urlo = new URLParse(url, {});

		if (['https:', 'http:'].indexOf(urlo.protocol) === -1) {
			setUrlStatus('please enter a http or https URL');
			return false;
		}

		if (!urlo.hostname) {
			setUrlStatus('please enter a valid URL');
			return false;
		}

		setUrlStatus('Checking URL...');
		let headers = {};

		if (token) {
			headers['Authorization'] = token;
		}

		fetch(url, {
			method: 'GET',
			headers
		}).then((response) => {
			if (response.status === 200) {
				response.json().then((info) => {
					setUrl(url.toString());
					setUrlStatus('Valid URL');
					console.log('got info', info);
					if (info.settings) {
						if (!label && info.settings.label) {
							setLabel(info.settings.label);
						}
						if (!updateTime && info.settings.updateTime) {
							setUpdateTime(String(info.settings.updateTime / 1000));
						}
						if (!updateDistance && info.settings.updateDistance) {
							setUpdateDistance(String(info.settings.updateDistance));
						}
						if (!maximumAge && info.settings.maximumAge) {
							setMaximumAge(String(info.settings.maximumAge));
						}
						if (!timeout && info.settings.timeout) {
							setTimeout(String(info.settings.timeout / 1000));
						}
					}
				});
			} else if (response.status === 403) {
				setUrlStatus('Authentication required');
			} else if (response.status === 404) {
				setUrlStatus('Error: page not found');
			} else {
				setUrlStatus('Error checking url: ' + response.status);
			}
		});
	};

	const numberOrNull = (number) => {
		if (typeof number !== 'number') {
			number = Number(number);
		}

		if (isNaN(number)) {
			return null;
		}

		return number;
	};

	const onSave = () => {
		if (props?.onSave && urlStatus === 'Valid URL') {
			props.onSave({
				...(props.share || {}),
				label,
				url,
				token,
				updateTime: numberOrNull(updateTime * 1000),
				updateDistance: numberOrNull(updateDistance),
				maximumAge: numberOrNull(maximumAge),
				timeout: numberOrNull(timeout * 1000)
			});
		}
	};

	return (
		<ScrollView style={styles.container}>
			<Text>Add details for new location share</Text>
			<Text style={globalStyles.label}>Label</Text>
			<TextInput style={globalStyles.input} value={label} onChangeText={setLabel} />
			<Text style={globalStyles.label}>Share URL</Text>
			<TextInput
				style={globalStyles.input}
				autoCorrect={false}
				autoCapitalize={false}
				value={url}
				onChangeText={updateUrl}
				onBlur={checkUrl}
				placeholder="https://example.com/share"
				keyboardType="url"
			/>
			<Text
				style={{
					fontSize: 12,
					marginLeft: 10,
					fontWeight: '500',
					color: urlStatus === 'Valid URL' ? '#090' : '#900'
				}}
			>
				{urlStatus}
			</Text>
			<Text style={globalStyles.label}>Authentication Token</Text>
			<TextInput
				style={globalStyles.input}
				autoCorrect={false}
				autoCapitalize={false}
				value={token}
				onChangeText={setToken}
			/>
			<Text style={globalStyles.hint}>Authentication token to include when sharing position</Text>
			<Text style={globalStyles.label}>Update Time</Text>
			<TextInput
				style={globalStyles.input}
				value={updateTime}
				onChangeText={setUpdateTime}
				keyboardType="numeric"
			/>
			<Text style={globalStyles.hint}>Interval between location updates in seconds</Text>
			<Text style={globalStyles.label}>Update Distance</Text>
			<TextInput
				style={globalStyles.input}
				value={updateDistance}
				onChangeText={setUpdateDistance}
				keyboardType="numeric"
			/>
			<Text style={globalStyles.hint}>Maximum distance between updates in meters</Text>
			<View
				style={{
					marginTop: 20,
					marginBottom: 20
				}}
			>
				<Button
					style={globalStyles.button}
					disabled={urlStatus !== 'Valid URL'}
					title="Save"
					onPress={onSave}
				/>
				{props.share ? (
					<Button style={globalStyles.button} color="#ed1111" title="Save" onPress={onDelete} />
				) : null}
			</View>
		</ScrollView>
	);
}

const styles = StyleSheet.create({
	container: {
		backgroundColor: '#fff',
		padding: 10,
		width: '100%'
	},
	status: {}
});
