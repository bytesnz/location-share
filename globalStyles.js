import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	iconButton: {
		padding: 5
	},
	icon: {
		height: 23,
		width: 23
	},
	label: {
		marginTop: 10,
		fontSize: 12
	},
	input: {
		borderColor: '#eee',
		width: '100%',
		borderWidth: 1,
		paddingVertical: 5,
		paddingHorizontal: 10,
		borderRadius: 5
	},
	hint: {
		fontSize: 12,
		fontStyle: 'italic',
		marginLeft: 10
	}
});
