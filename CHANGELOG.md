# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.1.0] - 2022-06-15

Initial version! Note:

* there is no status message showing when it is currently sharing a location
* background live location has not yet been tested

[0.1.1]: compare/v0.1.0...v0.1.1
[0.1.0]: tree/v0.1.0
