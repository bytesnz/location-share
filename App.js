import { StatusBar } from 'expo-status-bar';
import { Button, StyleSheet, Modal, Text, View } from 'react-native';
import { useState, useEffect } from 'react';
import Heading from './components/Header';
import List from './components/List';
import Form from './components/Form';
import Help from './components/Help';
import {
	useBackgroundPermissons,
	requestBackgroundPermissionsAsync,
	getBackgroundPermissionsAsync,
	requestForegroundPermissionsAsync,
	getForegroundPermissionsAsync,
	getCurrentPositionAsync
} from 'expo-location';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function App() {
	const [page, setPage] = useState('home');
	const [shares, setShares] = useState(null);
	const [activeShares, setActiveShares] = useState([]);
	const [backgroundPermission, setBackgroundPermission] = useState(null);
	const [foregroundPermission, setForegroundPermission] = useState(null);
	const [log, setLog] = useState('');
	const [error, setError] = useState(null);
	const [editingShare, setEditingShare] = useState(null);

	useEffect(() => {
		(async () => {
			const stored = await AsyncStorage.getItem('shares');

			if (stored === null) {
				setShares([]);
				return;
			}

			try {
				setShares(JSON.parse(stored));
			} catch {}
		})();
	}, []);

	const onAdd = () => {
		if (page !== 'home') {
			setPage('home');
			setEditingShare(null);
		} else {
			setPage('add');
		}
	};

	const onSave = (share) => {
		console.log('Saving', share);
		const index = shares.findIndex((s) => s.url === share.url);

		const newShares = [...shares];
		if (index === -1) {
			newShares.push(share);
		} else {
			newShares[index] = share;
		}
		setShares(newShares);
		AsyncStorage.setItem('shares', JSON.stringify(newShares));

		setPage('home');
		setEditingShare(null);
	};

	const updateShareTime = (updateShares) => {
		const newShares = [...shares];
		const date = new Date().toISOString();

		for (let i = 0; i < updateShares.length; i++) {
			const index = newShares.findIndex((s) => s.url === updateShares[i].url);

			if (index !== -1) {
				newShares[index].lastShare = date;
			}
		}

		AsyncStorage.setItem('shares', JSON.stringify(newShares));
		setShares(newShares);
	};

	const submitPosition = (share, position) => {
		const headers = {
			'Content-Type': 'application/json'
		};

		if (share.token) {
			headers['Authorization'] = share.token;
		}

		return fetch(share.url, {
			method: 'POST',
			headers,
			body: JSON.stringify({
				...position.coords,
				time: new Date(position.timestamp).toISOString()
			})
		});
	};

	const onEdit = (share) => {
		setEditingShare(share);
		setPage('add');
	};

	const onHelp = () => {
		setPage('help');
	};

	const onShare = async (share, type) => {
		if (type === 'live') {
			if (activeShares.indexOf(share.url) === -1) {
				addWatch(share);
			} else {
			}
			return;
		}

		let permission = foregroundPermission;

		if (!permission?.granted) {
			permission = await getForegroundPermissionsAsync();

			console.log(permission);

			if (!permission.granted && permission.canAskAgain) {
				console.log('asking for permission');
				permission = await requestForegroundPermissionsAsync();
				console.log('requested', permission);
			}

			setForegroundPermission(permission);
		}

		if (!permission?.granted) {
			setError({
				title: 'Geolocation Permission Issue',
				description:
					'Permission has been denied to get your location in the ' +
					'foreground, so this app cannot share your current position. ' +
					'If you want to, click the share postion again and grant ' +
					'foreground location permission.'
			});

			return;
		}

		const position = await getCurrentPositionAsync();

		console.log('got position', position);

		updateShareTime([share]);
		await submitPosition(share, position);
	};

	const addWatch = async (share) => {
		console.log('onWatch called');
		let permission = backgroundPermission;
		if (!permission?.granted) {
			permission = await getBackgroundPermissionsAsync();

			console.log(permission);

			if (!permission.granted && permission.canAskAgain) {
				console.log('asking for permission');
				permission = await requestBackgroundPermissionsAsync();
				console.log('requested', permission);
			}

			setBackgroundPermission(permission);
		}

		if (!permission?.granted) {
			setError({
				title: 'Geolocation Permission Issue',
				description:
					'Permission has been denied to get your location in the ' +
					'background, so this app cannot share your live position. ' +
					'If you want to, click the live postion share again and grant ' +
					'background location permission.'
			});

			return;
		}
	};

	return (
		<View style={styles.container}>
			<StatusBar style="auto" />
			<Heading page={page} onAdd={onAdd} onHelp={onHelp} />
			{page === 'add' ? (
				<Form share={editingShare} onSave={onSave} />
			) : page === 'help' ? (
				<Help />
			) : (
				<List shares={shares} liveShares={activeShares} onShare={onShare} onEdit={onEdit} />
			)}
			<Text>{log}</Text>
			<Modal animationType="slide" transparent={true} visible={error !== null}>
				<View
					style={{
						flex: 1,
						justifyContent: 'center',
						alignItems: 'center',
						padding: 20
					}}
				>
					<View
						style={{
							backgroundColor: '#fff',
							borderRadius: 20,
							borderColor: '#ccc',
							borderWidth: 1,
							padding: 20
						}}
					>
						<Text
							style={{
								fontWeight: '700',
								paddingVertical: 10
							}}
						>
							{error?.title || ''}
						</Text>
						<Text style={{}}>{error?.description || ''}</Text>
						<Button title="Ok" onPress={() => setError(null)} />
					</View>
				</View>
			</Modal>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		width: '100%',
		flex: 1,
		backgroundColor: '#fff',
		alignItems: 'flex-start',
		justifyContent: 'flex-start',
		paddingTop: 30
	}
});
