import { Linking, Text, TouchableOpacity, View } from 'react-native';
import packageDetails from '../package.json';

export default function Help() {
	return (
		<View
			style={{
				padding: 20
			}}
		>
			<Text
				style={{
					marginBottom: 10
				}}
			>
				location-share has been created so that a user can easily share their current or live
				location with any web app that provides the correct position API endpoints.
			</Text>
			<Text
				style={{
					marginBottom: 10
				}}
			>
				It was created as there is currenly no way to do it in a browser without keeping the phone
				awake and on the web site the location is being shared with.
			</Text>
			<Text>More information is available from:</Text>
			<TouchableOpacity
				style={{
					marginBottom: 20
				}}
				onPress={() => Linking.openURL(packageDetails.homepage)}
			>
				<Text
					style={{
						textDecorationLine: 'underline'
					}}
				>
					{packageDetails.homepage}
				</Text>
			</TouchableOpacity>
			<Text>
				{packageDetails.name} v{packageDetails.version}
			</Text>
		</View>
	);
}
