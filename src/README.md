# <!=package.json name> <!=package.json version>

<!=package.json description>

[![pipeline status](https://gitlab.com/bytesnz/<!=package.json name>/badges/main/pipeline.svg)](<!=package.json repository.url>/-/commits/main)
[![<!=package.json name> on NPM](https://bytes.nz/b/<!=package.json name>/npm)](https://npmjs.com/package/<!=package.json name>)
[![license](https://bytes.nz/b/<!=package.json name>/custom?color=yellow&name=license&value=AGPL-3.0)](<!=package.json repository.url>/-/blob/main/LICENSE)
[![developtment time](https://bytes.nz/b/<!=package.json name>/custom?color=yellowgreen&name=development+time&value=~3+hours)](<!=package.json repository.url>/-/blob/main/.tickings)
[![contributor covenant](https://bytes.nz/b/<!=package.json name>/custom?color=purple&name=contributor+covenant&value=v1.4.1+adopted)](<!=package.json repository.url>/-/blob/main/CODE_OF_CONDUCT.md)
[![support development](https://bytes.nz/b/<!=package.json name>/custom?color=brightgreen&name=support+development&value=$$)](https://liberapay.com/MeldCE)

<!=package.json name> allows you to share your location with applications that
you use. Simply download the app, then open a share link from your application
and <!=package.json name> will do the rest. Location sharing can be paused and
removed at any time. The application will provide a default update frequency,
but you can adjust it.

## Application Integration

For applications to use this app they must make available either a
[WebSocket][] connection or a [RESTful][] [API][] for <!=package.json name>
to communicate with


### Share URL

The share URL should be in the format

```
https://share.10-97.org/#<endpoint_url>#authorization=<token>
```

where `<endpoint_url>` is the endpoint URL (either http(s) or ws(s)) to use
for the share and `<token>` is the token to use for authorization. For example,

```
https://share.10-97.org/#https://example.com/share?type=position#authorization=53CR3T
```

The <!=package.json name> is registered to handle share URLs.

### Authorization

The authorization token will be passed as the
[`Authorization` header][authorization-header] for all HTTP requests.

### RESTful API

#### GET Method

The GET method will be used to authenticate the share and get the default
share settings. Invalid authorization tokens should be rejected with a
[403 HTTP status][403]. The share settings should be provided as
`application/json` with the following [JSON-schema][]:

```json
<!=schemas/settings.json>
```

For example:

```json
{
  "accuracy": "high",
  "updateTime": 60000,
  "updateDistance": 5,
  "maximumAge": 60000,
  "timeout": 15000
}
```

#### POST Method

The POST method will be used to post positions when they are available. The
positions will be submitted as `application/json` objects with the following
[JSON-schema][]:

```json
<!=schemas/position.json>
```

## Development

Feel free to post errors or feature requests to the project
[issue tracker](<!=package.json bugs.url>) or
[email](mailto:<!=package.json bugs.email>) them to us.
**Please submit security concerns as a
[confidential issue](<!=package.json bugs.url>?issue[confidential]=true)**

The source is hosted on [Gitlab](<!=package.json repository.url>) and uses
[eslint][], [prettier][], [lint-staged][] and [husky][] to keep things pretty.
As such, when you first [clone][git-clone] the repository, as well as
installing the npm dependencies, you will also need to install [husky][].

```bash
# Install NPM dependencies
npm install
# Set up husky Git hooks stored in .husky
npx husky install
```

The apps for both Android and iOS are developed using [expo][] framework
and tools. The expo project is available at
https://expo.dev/accounts/bytesnz/projects/location-share.

To develop on your local machine, you can either:

* develop, testing in the browser using `npm run web`
* develop, testing on a device by installing [expo go][expo-go] on the device
  and running
  ```sh
  npm run start
  ```

If you have issues connecting to the development server, you could try
using the `--tunnel` option, which will use the expo servers to create a
tunnel to your development instance

```sh
npm run start -- --tunnel
```

<!=CHANGELOG.md>

[husky]: https://typicode.github.io/husky
[eslint]: https://eslint.org/
[git-clone]: https://www.git-scm.com/docs/git-clone
[prettier]: https://prettier.io/
[lint-staged]: https://github.com/okonet/lint-staged#readme
[gitlab-ci]: https://docs.gitlab.com/ee/ci/
[json-schema]: https://json-schema.org/
[authorization-header]: https://developer.mozilla.org/docs/Web/HTTP/Headers/Authorization
[403]: https://developer.mozilla.org/docs/Web/HTTP/Status/403
[websocket]: https://developer.mozilla.org/docs/Web/API/WebSockets_API
[restful]: https://wikipedia.org/wiki/Representational_state_transfer#Applied_to_web_services
[api]: https://wikipedia.org/wiki/API
[expo]: https://expo.dev
[expo-go]: https://expo.dev/expo-go
