export default {
	type: 'object',
	title: 'DefaultShareOptions',
	properties: {
		label: {
			type: 'string',
			title: 'Share Label'
		},
		settings: {
			type: 'object',
			description: 'Default location share options',
			properties: {
				accuracy: {
					type: 'string',
					enum: ['high', 'low', 'any']
				},
				updateTime: {
					type: 'number',
					description: 'Interval between location updates in milliseconds',
					default: 60000
				},
				updateDistance: {
					type: 'number',
					description:
						'Update distance filter in meters. Specifies how often to update the location.'
				},
				maximumAge: {
					type: 'number',
					description: 'How old locations can be in milliseconds.'
				},
				timeout: {
					type: 'number',
					description: 'How long to wait for a location in ms.',
					default: 300000
				}
			}
		}
	}
};
