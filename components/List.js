import { Button, FlatList, Image, Text, TouchableOpacity, View } from 'react-native';
import globalStyles from '../globalStyles';

export default function List({ liveShares, onEdit, onShare, shares }) {
	const Share = ({ item }) => {
		return (
			<View
				style={{
					flexDirection: 'row'
				}}
			>
				<View
					style={{
						flexShrink: 1,
						flexGrow: 1
					}}
				>
					<Text style={{}} numberOfLines={1} ellipsizeMode="tail">
						{item.label || item.url}
					</Text>
					<Text
						style={{
							fontSize: 10,
							fontStyle: 'italic'
						}}
					>
						{liveShares?.indexOf(item.url) !== -1
							? 'Sharing live location'
							: item.lastShare
							? 'Last share: ' + new Date(item.lastShare).toLocaleString()
							: 'No position shared'}
					</Text>
				</View>
				<TouchableOpacity style={globalStyles.iconButton} onPress={() => onEdit?.(item)}>
					<Image style={globalStyles.icon} source={require('../assets/pencil.png')} />
				</TouchableOpacity>
				<TouchableOpacity style={globalStyles.iconButton} onPress={() => onShare?.(item, 'single')}>
					<Image style={globalStyles.icon} source={require('../assets/crosshairs-gps.png')} />
				</TouchableOpacity>
				<TouchableOpacity style={globalStyles.iconButton} onPress={() => onShare?.(item, 'live')}>
					<Image
						style={{
							...globalStyles.icon,
							opacity: liveShares?.indexOf(item.url) !== -1 ? 1 : 0.5
						}}
						source={require('../assets/crosshairs-repeat.png')}
					/>
				</TouchableOpacity>
			</View>
		);
	};

	if (!shares) {
		return (
			<View
				style={{
					padding: 10
				}}
			>
				<Text
					style={{
						fontStyle: 'italic'
					}}
				>
					Loading saved shares...
				</Text>
			</View>
		);
	} else if (!shares.length) {
		return (
			<View
				style={{
					padding: 10
				}}
			>
				<Text
					style={{
						fontStyle: 'italic'
					}}
				>
					No shares configured. Open a share URL or click the + to add one
				</Text>
			</View>
		);
	} else {
		return (
			<View
				style={{
					padding: 10,
					width: '100%'
				}}
			>
				<FlatList data={shares} renderItem={Share} keyExtractor={(s) => s.url} />
			</View>
		);
	}
}
