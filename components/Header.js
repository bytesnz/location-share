import { Text, TouchableOpacity, View } from 'react-native';
import globalStyles from '../globalStyles';

export default function Header(props) {
	return (
		<View
			style={{
				backgroundColor: '#ddd',
				width: '100%',
				flexDirection: 'row'
			}}
			accessibilityRole="toolbar"
		>
			<Text
				style={{
					fontSize: 16,
					fontWeight: '700',
					flexGrow: 1,
					padding: 10
				}}
			>
				location-share
			</Text>
			{props.page === 'help' ? null : (
				<TouchableOpacity style={globalStyles.iconButton} onPress={() => props.onHelp?.()}>
					<Text
						style={{
							paddingHorizontal: 5,
							fontWeight: '800',
							fontSize: 24
						}}
					>
						?
					</Text>
				</TouchableOpacity>
			)}
			<TouchableOpacity style={globalStyles.iconButton} onPress={() => props.onAdd?.()}>
				<Text
					style={{
						paddingHorizontal: 10,
						fontWeight: '800',
						fontSize: 24
					}}
				>
					{props.page === 'home' ? '+' : '×'}
				</Text>
			</TouchableOpacity>
		</View>
	);
}
