module.exports = {
	root: true,
	extends: [
    'eslint:recommended', 'prettier', 'plugin:react/recommended'
  ],
  plugins: [
    'react',
    'react-native'
  ],
	parserOptions: {
		sourceType: 'module',
		ecmaVersion: 2020,
    ecmaFeatures: {
      jsx: true
    }
	},
	ignorePatterns: ['index.d.ts'],
	env: {
		node: true,
    'react-native/react-native': true
	},
	rules: {
    'react/react-in-jsx-scope': 0,
		'no-fallthrough': 2,
		'no-case-declarations': 2,
		'no-console': [1, { allow: ['warn', 'error'] }],
		'no-debugger': 1,
		'no-warning-comments': [
			1,
			{ terms: ['xxx', 'todo', 'fixme', 'todo!!!'], location: 'anywhere' }
		],
		'require-jsdoc': 1
	},
	overrides: [
		{
			files: ['test/*.js'],
			rules: {
				'no-console': 0
			}
		}
	]
};
